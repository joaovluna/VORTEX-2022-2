package Q3;

import java.util.Random;

public class Poker {
    /** Desafio incompleto **/
    public static void main(String[] args) {
        String[] existe = new String[9];
        int exit = 0;

        String[] mesa = new String[5];
        String[] robo1 = new String[2];
        String[] robo2 = new String[2];

        int i = 0;

        distribuiCartas(i, mesa, robo1, robo2, existe, exit);

        System.out.println();

//        System.out.println("VALOR CARTA1 ROBO1: " + valorCarta(robo1[0]));
//        System.out.println("NAIPE CARTA1 ROBO1: " + naipeCarta(robo1[0]));
        System.out.println();
        System.out.println();
        String[] jogada1 = maiorCarta(robo1,mesa);

        System.out.print("MAO ROBO 1:");
        for(int j = 0; j < jogada1.length;j++) {
            System.out.print(" "+jogada1[j]);
        }

        String[] jogada2 = maiorCarta(robo2,mesa);

        System.out.println();
        System.out.println();
        System.out.print("MAO ROBO 2:");
        for(int j = 0; j < jogada2.length;j++) {
            System.out.print(" "+jogada2[j]);
        }

        System.out.println();
        System.out.println();
        System.out.println("PAR");
        String[] jogada3 = par(robo1,mesa);

        System.out.print("MAO ROBO 1:");
        for(int j = 0; j < jogada3.length;j++) {
            System.out.print(" "+jogada3[j]);
        }

        System.out.println();
        System.out.println();
        System.out.println("PAR");
        String[] jogada4 = par(robo2,mesa);

        System.out.print("MAO ROBO 2:");
        for(int j = 0; j < jogada4.length;j++) {
            System.out.print(" "+jogada4[j]);
        }

    }


    public static String[] par(String[] robo, String[] mesa) {
        String[] jogada = mesa.clone();
        int pos = 0;
        String aux;
        for(int m = 0; m < jogada.length; m++){

            for(int j = 1 + m; j < jogada.length ; j++ ) {
                if(valorCarta(jogada[m]) == valorCarta(jogada[j])) {
                    aux = jogada[j];
                    jogada[j] = jogada[m+1];
                    jogada[m+1] = aux;
                }
            }
        }

        for(int r = 0; r < robo.length; r++) {
            for(int m = 0; m < jogada.length-1; m++) {
                if(valorCarta(jogada[m]) == valorCarta(robo[r])) {
                    if(valorCarta(jogada[m+1]) != valorCarta(robo[r])) {
                        jogada[m+1] = robo[r];
                    }
                }
            }
        }

        return jogada;

    }

    public static String[] maiorCarta(String[] robo, String[] mesa) {
        String[] jogada = mesa.clone();

        for(int r = 0; r < robo.length;r++ ) {
            int menor = 15, pos = 0;

            for(int m = 0; m < jogada.length; m++) {
                if(valorCarta(jogada[m]) < menor) {
                    menor = valorCarta(jogada[m]);
                    pos = m;
                }
            }
            if(valorCarta(robo[r]) > menor) {
                jogada[pos] = robo[r];
            }
        }
        return jogada;
    }

    public static int valorCarta(String carta) {

        int valor;
        if(carta.substring(0,carta.length()-1).equals("A")) {
            valor = 14;
        } else if( carta.substring(0, carta.length() - 1).equals("K")) {
            valor = 13;
        } else if( carta.substring(0, carta.length() - 1).equals("Q")) {
            valor = 12;
        } else if( carta.substring(0, carta.length() - 1).equals("J")) {
            valor = 11;
        } else {
            valor = Integer.parseInt(carta.substring(0, carta.length() - 1));
        }

        return valor;
    }

    public static String naipeCarta(String carta) {
        return carta.substring(carta.length() - 1);
    }

    public static void distribuiCartas(int i, String[] mesa, String[] robo1, String[] robo2, String[] existe, int exit) {

        for (i = 0; i < existe.length; i++) {
            //preenchendo como vazio ""
            existe[i] = "";
        }

        for (i = 0; i < robo1.length; i++) {
            //distribuindo carta aos robos
            robo1[i] = confereCarta(geraCarta(), existe);
            if (robo1[i].equals(existe[exit])) {
                robo1[i] = confereCarta(geraCarta(), existe);
            } else {
                existe[exit] = robo1[i];
                exit++; //colocando no "existe" para nao gerar a mesma carta duas vezes
            }

            robo2[i] = confereCarta(geraCarta(), existe);
            if (robo2[i].equals(existe[exit])) {
                robo2[i] = confereCarta(geraCarta(), existe);
            } else {
                existe[exit] = robo2[i];
                exit++;
            }
        }

        for (i = 0; i < mesa.length; i++) {
            //colocando as cartas na mesa badumtssss
            mesa[i] = confereCarta(geraCarta(), existe);
            if (mesa[i].equals(existe[exit])) { //confere se ja saiu essa carta e gera uma carta nova, p/ n sair repetido
                mesa[i] = geraCarta();
            } else {    //se nao existe, entao cria normal e coloca a carta que saiu no vetor existe, e avança uma posição
                existe[exit] = mesa[i];
                exit++;
            }
        }

        System.out.print("MESA: ");
        for (i = 0; i < mesa.length; i++) {
            System.out.print(mesa[i] + " ");
        }
        System.out.println();
        System.out.print("ROBÔ 1: ");
        for (i = 0; i < robo1.length; i++) {
            System.out.print(robo1[i] + " ");
        }
        System.out.println();
        System.out.print("ROBÔ 2: ");
        for (i = 0; i < robo2.length; i++) {
            System.out.print(robo2[i] + " ");
        }
        System.out.println();
        System.out.print("MORTO: ");
        for (i = 0; i < existe.length; i++) {
            System.out.print(existe[i] + " ");
        }
    }
    public static String geraCarta() {
        String carta = "";
        Random random = new Random();

        int numero = random.nextInt(13) + 1;

        if (numero == 1) {
            carta = "A";
        } else if (numero > 1 && numero < 11) {
            carta = Integer.toString(numero);
        } else if (numero == 11) {
            carta = "J";
        } else if (numero == 12) {
            carta = "Q";
        } else if (numero == 13) {
            carta = "K";
        }

        double naipe = random.nextDouble();
        if (naipe < 0.25) { // C - COPAS
            carta += "C";
        } else if (naipe < 0.50) { // E - ESPADAS
            carta += "E";
        } else if (naipe < 0.75) { // O - OUROS
            carta += "O";
        } else { // P - PAUS
            carta += "P";
        }

        return carta;
    }

    public static String confereCarta(String carta, String[] existe) {
        String confere = "";

        for (int i = 0; i < existe.length; i++) {

            if (existe[i].equals(carta)) {
                return geraCarta();
            } else {
                confere = carta;
            }
        }

        return confere;
    }

}
