package Q1

fun main() {
    var inicio = 0
    while(inicio != -1) {
        var cont = 0
        var numero = 0
        var anterior = Integer.MAX_VALUE
        val repeticao = IntArray(4)
        println("Informe o número em algarismos romanos")
        val romanos = readLine() ?: ""
        for (a in romanos.uppercase()) {

            when { //converte o caracter pro respectivo numero, fazendo validação para algumas das regras dos algarismos romanos
                a.equals('I') && repeticao[0] < 2 ->{
                    numero = 1
                    if(anterior == 1) {
                        repeticao[0]++
                        println(repeticao[0])
                    } else {
                        repeticao[0] = 0
                    }
                }

                a.equals('V') && anterior != 5 -> numero = 5

                a.equals('X') && repeticao[1] < 2 -> {
                    numero = 10
                    if(anterior == 10 ) {
                        repeticao[1]++
                    } else {
                        repeticao[1] = 0
                    }
                }

                a.equals('L') && anterior != 50 -> numero = 50

                a.equals('C') && repeticao[2] < 2 -> {
                    numero = 100
                    if(anterior == 100 ) {
                        repeticao[2]++
                    } else {
                        repeticao[2] = 0
                    }
                }

                a.equals('D') && anterior != 500 -> numero = 500

                a.equals('M') && repeticao[3] < 2 -> {
                    numero = 1000
                    if(anterior == 1000) {
                        repeticao[3]++
                    } else {
                        repeticao[3] = 0
                    }
                }
                else -> {
                    numero = Integer.MIN_VALUE
                } // representando q nao existe essa letra em numeros romanos ou que está ferindo alguma das regras dos algarismos romanos
            }

            if (numero == Integer.MIN_VALUE){
                println("ERRO: ${a.uppercase()} excede as regras dos algarismos romanos, gentileza tentar novamente")
                cont = Integer.MIN_VALUE
                inicio = -1 //- comentar/descomentar para isso ser critério de parada do programa
                break

            } else {

                if(anterior < numero ) {
                    cont = cont - anterior + numero - anterior
                } else {
                    cont += numero
                }
                anterior = numero
            }

        }
        println("${romanos.uppercase()} => $cont")

    }


}
