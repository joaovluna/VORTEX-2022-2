package Q2

import java.util.*


fun main() {

    println("Informe a quantidade de estrelas: ")
    var estrelas = readLine()?.toInt() ?: 0

    if (estrelas < 4) {
        estrelas = 4
    } else if (estrelas > 8) {
        estrelas = 8
    }
    val constelacao = Array(estrelas) { IntArray(estrelas) }

    for (i in 0 until estrelas) { //preenchendo aleatoriamente
        for (j in 0 until estrelas) {

            if (Random().nextDouble() < .5) {
                constelacao[i][j] = 1
                constelacao[j][i] = 1
            }
            constelacao[j][j] = 0
        }
    }

    println(""" 
        Informe a 1ª estrela, para verificar ligação:
        Escolha entre 0 à ${estrelas-1}
    """.trimIndent())
    val estrela1 = readLine()?.toInt() ?: 0
    println(""" 
        Informe a 2ª estrela, para verificar ligação:
        Escolha entre 0 à ${estrelas-1}
    """.trimIndent())
    val estrela2 = readLine()?.toInt() ?: 0

    for (i in 0 until estrelas) { //preenchendo aleatoriamente
        print("[")
        for (j in 0 until estrelas) {
            print(" " + constelacao[i][j] + " ")
        }
        println("]")
    }

    if (constelacao[estrela1][estrela2] == 1) {
        println("há ligação")
    } else {
        println("não há ligação")
    }
}

